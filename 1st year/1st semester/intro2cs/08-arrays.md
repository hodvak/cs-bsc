# מערכים #
מערך הוא רשימה של משתנים  
## הגדרה ##
את הרשימה נדיר כך
```cpp
{type} {name}[{size}];
```
לדוגמה אם נרצה רשימה של 5 מספרים שלמים
```cpp
int nums[5];
```
עכשיו קיימים לנו 5 מספרים שלמים

## פנייה לתא ##
נסכל על הדוגמה
```cpp
int nums[5];
```
פנייה לאחד התאים תהיה לפי המספר שלו כאשר מתחילים מ0.  
כלומר יש 5 תאים:  
0,1,2,3,4  
את המספר שמים בתוך סוגריים מרובעים.  
```cpp
int nums[5];
num[0] = 1;
num[1] = 2;
num[2] = 4;
num[3] = 1;
num[4] = 2;
```
הערכים בnum יהיו:  
1,2,4,1,2

## העברת מערך כפרמטר ##
כך נראת פונקציה שמקבלת מערך כפרמטר
```cpp
{type} {func_name} ({arr_type} {arr_name}[{optional: size])
```

לדוגמה
```cpp
int sum_of_array(int arr[5])
{
    int sum = 0;
    for(int i = 0; i < 5; ++i)
    {
        sum += arr[i];
    }
    return sum;
}
```
בדרך כלל רצוי לקבל את המערך ואת הגודל כשני פרמטרים, כך ניתן לקבל מערך בכל גודל:  
```cpp
int sum_of_array(int arr[], int length)
{
    int sum = 0;
    for(int i = 0; i < length; ++i)
    {
        sum += arr[i];
    }
    return sum;
}
```
### פרמטר הפניה ###
אין צורך להוסיף את סימן ה`&` כאשר מעברים את המערך כפרמטר הפניה, הוא תמיד עובר כפרמטר הפניה
לדוגמה
```cpp
void zero_array(int arr[], int length)
{
    for(int i = 0; i < length; ++i)
    {
        arr[i] = 0;
    }
}
```
הפעולה מאפסת את ערכי המערך

### const ###
אם אין בכוונתכם לשנות את הערכים במערך יש להשתמש ב`const` לפני הסוג של המערך
```cpp
int sum_of_array(const int arr[], int length)
{
    int sum = 0;
    for(int i = 0; i < length; ++i)
    {
        sum += arr[i];
    }
    return sum;
}
```
