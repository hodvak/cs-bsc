# לולאות #
## while ##
לולאת while נראית כך:
```cpp
while(תנאי)
{
    קוד
}
```
כל עוד התנאי קורה יופעל הקוד לדוגמה
```cpp
int a = 1;
while (a < 10)
{
    a = a*2;
}
cout << a;
```
הדבר הראשון שקורה הוא שa=1
אחר כך בודקים את התנאי, אם התנאי מתקיים עושים את קטע הקוד שנמצא בגוף הלולאה ואחר כך בודקים שוב את התנאי
התנאי מתקיים ולכן נשנה את a להיות פי 2.  
עכשיו a=2
התנאי עדיין מתקיים ולכן מפעילים שוב את הקוד  
עכשיו a=4  
התנאי עדיין מתקיים ולכן מפעילים שוב את הקוד  
עכשיו a=8  
התנאי עדיין מתקיים ולכן מפעילים שוב את הקוד  
עכשיו a=16
התנאי כבר לא מתקיים ולכן יצאים מהלולאה
מדפיסים את a  
כל קטע התוכנית ידפיס 16

## for ##
לולאת while שנראית באופן הבא:
```cpp
{type} {var_name} = {value};
while({condition})
{
    {code}
    {var_name} = {new value};
}
```
ניתן להפוך לצורה הבאה:
```cpp
for({type} {var_name} = {value}; {condition}; {var_name} = {new value};)
{
    {code}
}
```
לדוגמה: לולאה שתדפיס את כל המספרים בין אחד ל10:
```cpp
int a = 1;
while (a <= 10)
{
    cout << a;
    a++;
}
```
ניתן להמיר לקוד הבא:
```cpp
for(int a=1; a<=10; a++)
{
    cout << a;
}
```
שימו לב שכל לולאת for ניתן להמיר לולאת while ולהפך.  
בדרך כלל כאשר ניתן לדעת מראש כמה פעמים נבצע את הקוד, נשתמש בלולאת for, כאשר לא ניתן לדעת מראש, נשתמש בwhile.  
דוגמאות לשימוש בfor:  
* הדפסת כל המספרים בין 1 ל 10
* קליטת 10 מספרים מהמשתמש

דוגמאות לשימוש בwhile:  
* קליטת מספרים מהמשתמש עד קבלת -1
