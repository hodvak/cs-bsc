# עבודה עם קבצים #

## `cin` / `cout` ##

### `cout` ###

`cout` היא פקודה לקליטת נתונים בעזרת 'הפלט הסטנדרטי'

דוגמה בסיסית לפלט למשתמש:
```cpp
#include <iostream>

using std::cout;

int main()
{
    cout << "Hello World";
    return EXIT_SUCCESS;
}
```
את הקובץ קימפלנו לקובץ ההרצה `exe`
על מנת להריץ את הפקודה מהטרמינל נשתמש בפקודה:
```console
$ ./exe
Hello World
```
הפלט הסטנדרתי יהיה לטרמינל (כמו שניתן לראות). אך ניתן גם להגדיר את הפלט הסטנדרטי לכתיבה לקובץ.  
נריץ את התוכנית בעזרת הוספת "חץ" לתןך קובץ הקובץ שאליו נרצה לכתוב
```console
$ ./{file_to_run} > {stdin}
```

לדוגמה:  
נפעיל את הפקודה הבאה:
```console
$ ./exe > my_new_file.txt
```
שימו לב שבטרמינל לא נראה פלט  
בקובץ my_new_file.txt יראה כך:
```txt
Hello World
```

### `cin` ###
`cin` היא פקודה לקליטת נתונים בעזרת 'הקלט הסטנדרטי'

דוגמה בסיסית לקליטת מספר מהמשתמש:
```cpp
#include <iostream>

using std::cin;

int main()
{
    int num;
    cin >> num;
    return EXIT_SUCCESS;
}
```
את הקובץ קימפלנו לקובץ ההרצה `exe`
על מנת להריץ את הפקודה מהטרמינל נשתמש בפקודה:
```console
$ ./exe
```
את הקלט נכניס בעזרת המקלדת

`cin` הוא הקלט הסטנדרטי, בדרך כלל נקח מהמקלדת. אך ניתן גם להגדיר את הקלט הסטנדרטי כקובץ.  
נריץ את התוכנית בעזרת הוספת "חץ" לתןך קובץ ההרצה
```console
$ ./{file_to_run} < {stdin}
```
לדוגמה:  
קיים לנו הקובץ הבא:  
in.txt
```txt
123
```
נרצה להכניס אותו במקום הקלט הסטנדרטי. נריץ את התוכנית בעזרת הפעולה הבאה:  
```console
$ ./exe < in.txt
```

#### `bad`/`good`/`eof`/`fail` ####
לפעמים, כאשר ננסה לקלוט מהקלט הסטנדרטי, יכולה להיות בעיה.  
הבעיה לא תזרוק את התוכנית, היא פשוט תגרום לכך שלא יהיה יותר ניתן לקרוא נתונים, וכל פעולת קליטה לאחר מכן לא תפעל.
##### `eof` (end of file) #####
אם ננסה לקרוא מהקלט הסטנדרטי, למרות שהגענו לסיום הקובץ לדוגמה עבור הקובץ
```cpp
#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    int num1 = 1, 
        num2 = 2,
        num3 = 3;

    cin >> num1;
    cout << cin.eof() << endl;
    cout << num1 << endl;
    cin >> num2;
    cout << cin.eof() << endl;
    cout << num2 << endl;
    cin >> num3;
    cout << cin.eof() << endl;
    cout << num3 << endl;
    
    return EXIT_SUCCESS;
}
```
והקובץ inp.txt
```txt
100
200
```
כך תראה ההרצה של קובץ ההרצה:
```console
$ ./exe < inp.txt
0
100
0
200
1
3
```
כפי שניתן לראות, לא נסגרה התוכנית, אך ניתן לראות שלא עפה התוכנית
והeof השתנה לtrue

ניתן להשתמש בזה על מנת לקורא נתונים מקובץ עד שהקובץ נגמר.  
דוגמה לקוד שסוכם את כל המספרים שהוא מקבל ומדפיס את התוצאה:  
```cpp
#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    int sum = 0;
    int num;

    cin >> num; // get number from the stdin (standart input)
    
    while(!cin.eof()) // while the last number we insert to num was good and we didn't try to read after the end of the file
    {
        sum += num; // add the number to the sum
        cin >> num; // get another number (or fail to do it if we got to the end of the file)
    }

    cout << sum << endl; // we got out of the loop so we passed all the file
    
    return EXIT_SUCCESS;
}
```
##### `fail` ####
כשילון שלא נובע מסוף הקובץ
```cpp
#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    int num1 = 1, 
        num2 = 2,
        num3 = 3;

    cin >> num1;
    cout << cin.fail() << endl;
    cout << num1 << endl;
    cin >> num2;
    cout << cin.fail() << endl;
    cout << num2 << endl;
    cin >> num3;
    cout << cin.fail() << endl;
    cout << num3 << endl;
    
    return EXIT_SUCCESS;
}
```
והקובץ inp.txt
```txt
100
200
Hod
```
כך תראה ההרצה של קובץ ההרצה:
```console
$ ./exe < inp.txt
0
100
0
200
1
3
```
כפי שניתן לראות, לא נסגרה התוכנית, אך ניתן לראות שלא עפה התוכנית
fail השתנה לtrue  
הערך יהפוך לfail לא רק בעקבות "טעות לוגית" (כמו מה שראינו).
אלא גם אם קרה משהו שלא ניתן לתקן בקלט (הקובץ נמחק תוך כדי קריאה, המקלדת התנתקה וכו')

##### `bad` #####
`cin.bad()` 
יהיה true רק כאשר קרה משהו שלא ניתן לתקן בקלט

#### `good` ####
כל עוד לא קרתה שום בעיה וכל הקלטים עד עכשיו התקבלו כמו שצריך,   
`cin.good()`   
יחזיר true

#### `clear` ####
אם הייתה בעיה שניתן לתקן  
(לא bad או eof)  
ניתן להגיד לcin שאנחנו נטפל בבעיה על ידי  
`cin.clear()`  
לדוגמה:  
```cpp
int main()
{
    int sum = 0;
    int num;
    char arr[1024];

    cin >> num; // get number from the stdin (standart input)
    
    while(cin.good()) // while the last number we insert to num was good
    {
        sum += num; // add the number to the sum
        cin >> num; // get another number (or fail for any reason)
    }

    if(cin.bad() || cin.eof()) // if we got a fail not for logical reason (text and not int)
    {
        cout << sum << endl; // we got out of the loop so we passed all the file
    }
    else{
        cin.clear();
        cin >> setw(1024) >> arr;
        cout << "You have to enter number, WTF is '" << arr << "'?" << endl;
    }
    

    return EXIT_SUCCESS;
}
```
והקובץ in.txt:
```txt
100
200
Hod
```
נקבל:  
```txt
You have to enter number, WTF is 'Hod'?
```
לעומת זאת עבור הקובץ:  
```txt
100
200
300
```
נקבל:  
```txt
600
```

## קבצים ##
אז למה חזרנו על cin ו cout?
בגלל שקבצים עובדים באותה צורה.

### יצירת קובץ ###
כדי ליצור קובץ צריך לדעת באיזה דגלים משתמשים:  
|דגל|משמעות|
|------|----|
|`ios::in`  (input)  |  נרצה לקחת מידע מהקובץ |
|`ios::out` (out)    | נרצה להכניס מידע לקובץ |
|`ios::app` (append) | אם כותבים לקובץ אז רק להוסיף לקובץ ולא לדרוס ולכתוב מחדש |

את הדגלים נחבר בעמצאות `|`

```cpp
#include <iostream>
#include <fstream>

using std::fstream;
using std::ios;
using std::endl;

int main () {

    // this is the file, all the methods on the file will be with it
    fstream myfile;

    // open file with the flags we want (just for output)
    myfile.open ("example.txt" , ios::out);

    // write to the file (as `cout`)
    myfile << "Hello World" << endl;

    // don't forget to close the file when finish to write to the file
    myfile.close();

    return EXIT_SUCCESS;
}
```
אם נריץ את הדוגמה, התוכנית תיצור קובץ או תדרוס את הקובץ הקיים ותכתוב בתוכו  
```txt
Hello World
```

הדוגמה הבאה כותבת לתוך הקובץ, בסוף שלו, בלי לדרוס את מה שכבר כתוב שם:  
```cpp
#include <iostream>
#include <fstream>

using std::fstream;
using std::ios;
using std::endl;

int main () {

    // this is the file, all the methods on the file will be with it
    fstream myfile;

    // open file with the flags we want (just for output)
    myfile.open ("example.txt" , ios::out | ios::app);

    // write to the file (as `cout`)
    myfile << "Hello World" << endl;

    // don't forget to close the file when finish to write to the file
    myfile.close();

    return EXIT_SUCCESS;
}
```
